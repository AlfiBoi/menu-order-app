import React, { useState } from 'react';
import Menu from './Menu';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';

function Header() {
    const [openSideMenu, setOpenSideMenu] = useState(false);

    return (
        <div id="toasted-header" className="Header__container">
            <Menu isMenuOpen={openSideMenu} setIsMenuOpen={setOpenSideMenu} />
            <AppBar position="static" className="Header__appBar">
                <Toolbar>
                    <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        onClick={(e) => {
                            e.preventDefault();
                            setOpenSideMenu(!openSideMenu);
                        }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography gutterBottom variant="h4" component="h2" className="Header__title">
                        Delivery App
                    </Typography>
                    <Button
                        color="inherit"
                        variant="outlined"
                        className="Header__button--login"
                    >
                        Login
                    </Button>
                    <Button
                        color="inherit"
                        variant="outlined"
                        className="Header__button--register"
                    >
                        Register
                    </Button>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default Header;

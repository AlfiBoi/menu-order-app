import React from 'react';
import cx from 'classnames';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

function OrderListCard({orderName, size, cheese, quantity, totalPrice, className, onClickRemove, onClick }) {
    const pizzaSize = {
        "S": "Small",
        "M": "Medium",
        "L": "Large",
    };

    const pizzaCheese = {
        true: "Extra Cheese",
        false: "No Extra Cheese",
    };

    const pizzaName = {
        "Chonky_Chicken": "Chonky Chicken",
        "Beef_Barberque": "Beef Barbeque",
        "Hawking_Hawaiian": "Hawking Hawaiian",
        "Margeret_Margherita": "Margeret's Margherita",
        "Vegan_Villa_Vista": "Vegan Villa Vista",
    };

    return (
        <Card className={cx(className)} onClick={onClick}>
            <CardActionArea>
                <CardContent className={cx('OrderCard__content')} >
                    <Typography gutterBottom variant="h5" component="h2">
                        {pizzaName[orderName]}
                    </Typography>
                    <Typography
                        variant="h6"
                        color="textSecondary"
                        component="h3"
                    >
                        {pizzaSize[size]}
                    </Typography>
                    <Typography
                        variant="h6"
                        color="textSecondary"
                        component="h3"
                    >
                        {pizzaCheese[cheese]}
                    </Typography>
                    <Typography
                        variant="h6"
                        color="textSecondary"
                        component="h3"
                    >
                        {quantity}
                    </Typography>
                    <Typography
                        variant="h6"
                        color="textSecondary"
                        component="h3"
                    >
                        {totalPrice}
                    </Typography>
                    <div className="OrderCard__button--wrapper">
                        <Button  variant="contained" onClick={onClickRemove} className="OrderCard__button--color">
                            Remove
                        </Button>
                    </div>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}

export default OrderListCard;

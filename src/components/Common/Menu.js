import React from 'react';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import { useHistory } from "react-router-dom";

function Menu({ isMenuOpen, setIsMenuOpen }) {
    let history = useHistory();

    const toggleMenu = (open) => (event) => {
        if (
            event &&
            event.type === 'keydown' &&
            (event.key === 'Tab' || event.key === 'Shift')
        ) {
            return;
        }

        setIsMenuOpen(open);
    };

    const redirect = (path) =>{
        if (path==="Home"){
            history.push("/");
        }
        else if (path==="Menu"){
            history.push("/menu");
        }
        else if (path==="Orders"){
            history.push("/order");
        }

    }

    const list = () => (
        <div role="presentation">
            <List>
                {['Home', 'Menu', 'Orders'].map((text, index) => (
                    <ListItem button key={text} className="Menu__listItem" onClick={()=>redirect(text)}>
                        <ListItemIcon>
                            {index % 2 === 0 ? (
                                <InboxIcon className="Menu__icon" />
                            ) : (
                                <MailIcon className="Menu__icon" />
                            )}
                        </ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
                <Divider />
            </List>
        </div>
    );

    return (
        <div id="toasted-side-menu">
            <React.Fragment key={'left'}>
                <SwipeableDrawer
                    anchor={'left'}
                    open={isMenuOpen}
                    onClose={toggleMenu(false)}
                    onOpen={toggleMenu(true)}
                    className="Menu__container"
                >
                    {list()}
                </SwipeableDrawer>
            </React.Fragment>
        </div>
    );
}

export default Menu;

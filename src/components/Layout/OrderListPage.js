import React ,{useContext, useState, useEffect}from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { useHistory } from "react-router-dom";
import Button from '@material-ui/core/Button';
import {Header, OrderListCard} from '../Common'
import {OrderContext, SnaccBarContext} from '../../contexts';

function OrderListPage() {
    let history = useHistory();
    const Order = useContext(OrderContext);
    const SnaccBar = useContext(SnaccBarContext);
    const [totalOrderPrice, setTotalOrderPrice] = useState(0);
    const [orderList, setOrderList] = useState(Order.orderList);
    
    //redirect
    const previousPage = () => {
        history.push("/menu");
    }
    const nextPage = () => {
        history.push("/success");
        Order.orderList = [];
        Order.setDeliveryMethod("");
        SnaccBar.setOpenSnacc(true);
    }

    const removeOrder = (index) => {
        let tempOrder = orderList;
        if (orderList.length > 0){
            tempOrder.splice(index, 1);  
        }
        setOrderList(tempOrder);
        calculateTotalPrice(orderList);
    }

    const calculateTotalPrice = (orderArr) => {
        let totalPrice = 0;
        orderArr.forEach(item => {
            totalPrice += item.price_total
        });
        setTotalOrderPrice(totalPrice);
    }
    
    useEffect(() => {
        calculateTotalPrice(orderList);
    }, [orderList]);

    const orderListMap = (orderArr) => {
        if (orderArr){
            return (
                orderArr.map((item, index) =>
                    <div className="OrderListPage__cardDivider" key={index}>
                        <OrderListCard
                            orderName={item.name}
                            size={item.size}
                            quantity={item.quantity}
                            cheese={item.extraCheese}
                            totalPrice={item.price_total}
                            onClickRemove={() => removeOrder(index)}
                        />
                    </div>
                )
            );
        }
    }

    return (
        <div>
          <Header></Header>
          <Container className="LandingPage__container">
                <Typography variant="h4" className="LandingPage__title">
                    Please review your order
                </Typography>
                <div className="OrderListPage__tableHeader">
                    <Typography gutterBottom variant="h5" component="h2">
                        Order Name
                    </Typography>
                    <Typography
                        variant="h6"
                        color="textSecondary"
                        component="h3"
                    >
                        Pizza Size
                    </Typography>
                    <Typography
                        variant="h6"
                        color="textSecondary"
                        component="h3"
                    >
                        Extra Cheese
                    </Typography>
                    <Typography
                        variant="h6"
                        color="textSecondary"
                        component="h3"
                    >
                        Quantity
                    </Typography>
                    <Typography
                        variant="h6"
                        color="textSecondary"
                        component="h3"
                    >
                        Total Price
                    </Typography>
                </div>
                <Grid container spacing={7} justify="center">
                    <Grid item xs={8} sm={12}>  
                        {orderListMap(orderList)} 
                    </Grid>
                    <div className="OrderListPage__info">
                        <Typography variant="h4" className="OrderListPage__info--text">
                            Total price: {totalOrderPrice}
                        </Typography>
                        <Typography variant="h4" className="OrderListPage__info--text">
                            Method of delivery: {Order.deliveryMethod}
                        </Typography>
                    </div>
                </Grid>
                <div className="OrderListPage__button">
                    <Button  onClick={previousPage}>
                        Back
                    </Button>
                    <Button variant="contained" className="MenuModal__button--color" onClick={nextPage}>
                        Order
                    </Button>
                </div>
            </Container>
        </div>
    );
}

export default OrderListPage;

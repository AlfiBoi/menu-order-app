import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { useHistory } from "react-router-dom";
import {Header} from '../Common';

function Success() {
    let history = useHistory();
    //redirect
    const homePage = () => {
        history.push("/");
    }
    return (
        <div>
            <Header></Header>
            <Container className="LandingPage__container">
                <Typography variant="h4" className="LandingPage__title">
                    Your order has been taken successfully :D
                </Typography>
                <div className="OrderListPage__button">
                    <Button variant="contained" className="MenuModal__button--color" onClick={homePage}>
                        Home
                    </Button>
                </div>
            </Container>
        </div>
    );
}

export default Success;

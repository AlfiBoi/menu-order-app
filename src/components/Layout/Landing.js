import React ,{useContext}from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { useHistory } from "react-router-dom";
import {Header, CardInfo} from '../Common'
import {OrderContext} from '../../contexts';

function Landing() {
    let history = useHistory();

    const Order = useContext(OrderContext);

    const redirectPage = (type) =>{
        if(type === "delivery"){
            Order.setDeliveryMethod('delivery');
        }
        else{
            Order.setDeliveryMethod('pickup');
        }
        history.push("/menu");
    }
    return (
        <div>
          <Header></Header>
          <Container className="LandingPage__container">
                <Typography variant="h4" className="LandingPage__title">
                    Please choose your order options
                </Typography>
                <Grid container spacing={7} justify="center">
                    <Grid item xs={8} sm={6}>
                    <CardInfo
                        title="Delivery"
                        shortDescription="to be delivered to your doorstep"
                        className="LandingPage__card"
                        onClick={()=>redirectPage('delivery')}
                    />
                    </Grid>
                    <Grid item xs={8} sm={6}>
                    <CardInfo
                        title="Pickup"
                        shortDescription="to be picked up by yours truly"
                        className="LandingPage__card"
                        onClick={()=>redirectPage('pickup')}
                    />
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
}

export default Landing;

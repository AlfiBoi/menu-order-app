import React, {useContext, useState, useRef} from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import {Header, CardInfo} from '../Common';
import {OrderContext} from '../../contexts';
import { useHistory } from "react-router-dom";
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

function MenuPage() {
    let history = useHistory();
    const Order = useContext(OrderContext);
    const [openModal, setOpenModal] = useState(false);
    const refContainer = useRef(null);

    const [selectedPizza, setSelectedPizza] = useState("");
    const [pizzaSize, setPizzaSize] = useState("S");
    const [extraCheese, setExtraCheese] = useState(false);
    const [quantity, setQuantity] = useState(0);
    const [onePizzaPrice, setOnePizzaPrice] = useState(0);

    //redirect
    const previousPage = () => {
        history.push("/");
    }
    const nextPage = () => {
        history.push("/order");
    }

    //open or close modal
    const handleOpenModal = (pizzaType) => {
        //todo: set pizza price
        setOnePizzaPrice(Order.pizzaPriceInfo[pizzaType])
        setSelectedPizza(pizzaType);
        setOpenModal(true);
    };

    const handleCloseModal = () => {
        //reset the pizza stuff
        setSelectedPizza("");
        setPizzaSize("S");
        setQuantity(0);
        setExtraCheese(false);

        setOpenModal(false);
    };


    //handling pizza orders and type
    const handlePizzaSizeChange = (event) => {
        setPizzaSize(event.target.value);
    };

    const handleExtraCheeseChange = (event) =>{
        setExtraCheese(event.target.value);
    }

    const handleQuantityChange = (event) =>{
        setQuantity(event.target.value);
    }
    
    const handleSubmit = (event)=> {
        if (quantity > 0){
            let orderInfoObject = {
                name: selectedPizza,
                size: pizzaSize,
                extraCheese: extraCheese,
                quantity: quantity,
                price_one: onePizzaPrice,
                price_total: onePizzaPrice * quantity,
            }
            Order.orderList.push(orderInfoObject);
        }
        handleCloseModal();
        event.preventDefault();
    }

    const body = (
        <Paper className="MenuModal__container">
            <h2 id="simple-modal-title">Customize your pizza!</h2>
            <form onSubmit={(e)=>handleSubmit(e)} ref={refContainer} className="MenuModal__container--form">
                <FormControl className="MenuModal__formControl">
                    <InputLabel id="pizza-size-input-label">Pizza size</InputLabel>
                    <Select
                        labelId="pizza-size-input-label"
                        id="pizza-size-input-select"
                        value={pizzaSize}
                        onChange={handlePizzaSizeChange}
                    >
                        <MenuItem value={"S"}>Small</MenuItem>
                        <MenuItem value={"M"}>Mediuum</MenuItem>
                        <MenuItem value={"L"}>Large</MenuItem>
                    </Select>
                </FormControl>
                <div className="MenuModal__divider"></div>
                <FormControl className="MenuModal__formControl">
                    <InputLabel id="pizza-cheese-input-label">Extra Cheese</InputLabel>
                    <Select
                        labelId="pizza-cheese-input-label"
                        id="pizza-cheese-input-select"
                        value={extraCheese}
                        onChange={handleExtraCheeseChange}
                    >
                        <MenuItem value={true}>Yes</MenuItem>
                        <MenuItem value={false}>No</MenuItem>
                    </Select>
                </FormControl>
                <div className="MenuModal__divider"></div>
                <FormControl className="MenuModal__formControl">
                    <TextField
                        id="standard-textarea"
                        label="Quantity of Pizza"
                        placeholder="420"
                        multiline
                        onChange={(e)=>handleQuantityChange(e)}
                    />
                </FormControl>
                <div className="MenuModal__button">
                    <Button  onClick={handleCloseModal}>
                        Close
                    </Button>
                    <Button variant="contained" type="submit" value="Submit" className="MenuModal__button--color">
                        Submit
                    </Button>
                </div>
            </form>
        </Paper>
    );
    

    return (
        <div>
          <Header></Header>
          <Container className="LandingPage__container">
                <Typography variant="h4" className="LandingPage__title">
                    Please Chooose your Menu
                </Typography>
                <Grid container spacing={7} justify="center">
                    <Grid item xs={8} sm={6}>
                    <CardInfo
                        title="Chonky Chicken"
                        shortDescription="10$"
                        onClick={()=>handleOpenModal("Chonky_Chicken")}
                        className="MenuPage__card"
                    />
                    </Grid>
                    <Grid item xs={8} sm={6}>
                    <CardInfo
                        title="Beef Barberque"
                        shortDescription="12$"
                        onClick={()=>handleOpenModal("Beef_Barberque")}
                        className="MenuPage__card"
                    />
                    </Grid>
                </Grid>
                <Grid container spacing={7} justify="center">
                    <Grid item xs={8} sm={6}>
                    <CardInfo
                        title="Hawking Hawaiian"
                        shortDescription="10$"
                        onClick={()=>handleOpenModal("Hawking_Hawaiian")}
                        className="MenuPage__card"
                    />
                    </Grid>
                    <Grid item xs={8} sm={6}>
                    <CardInfo
                        title="Margeret's Margherita"
                        shortDescription="8$"
                        onClick={()=>handleOpenModal("Margeret_Margherita")}
                        className="MenuPage__card"
                    />
                    </Grid>
                </Grid>
                <Grid container spacing={7} justify="center">
                    <Grid item xs={8} sm={6}>
                    <CardInfo
                        title="Vegan Villa Vista"
                        shortDescription="8$"
                        onClick={()=>handleOpenModal("Vegan_Villa_Vista")}
                        className="MenuPage__card"
                    />
                    </Grid>
                </Grid>
            </Container>
            <Modal
                open={openModal}
                onClose={handleCloseModal}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                className="MenuModal__position"
            >
                {body}
            </Modal>
            <div className="MenuPage__button">
                <Button  onClick={previousPage}>
                    Back
                </Button>
                <Button variant="contained" className="MenuModal__button--color" onClick={nextPage}>
                    Checkout
                </Button>
            </div>
        </div>
    );
}

export default MenuPage;

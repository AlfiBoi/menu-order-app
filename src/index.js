import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {OrderProvider, SnaccBarProvider} from './contexts/';

ReactDOM.render(
  <React.StrictMode>
    <OrderProvider>
      <SnaccBarProvider>
        <App />
      </SnaccBarProvider>
    </OrderProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

import React, {
    createContext,
    useState,
    useEffect,
} from 'react';

export const OrderContext = createContext();

export function useOrder() {

    const [deliveryMethod, setDeliveryMethod] = useState(null);
    const orderList = [];
    const pizzaPriceInfo = {
        "Chonky_Chicken": 10,
        "Beef_Barberque": 12,
        "Hawking_Hawaiian": 10,
        "Margeret_Margherita": 8,
        "Vegan_Villa_Vista":8,
    };
    
    useEffect(() => {
    }, [orderList]);

    return {
        deliveryMethod,
        setDeliveryMethod,
        orderList,
        pizzaPriceInfo,
    };
}

export const OrderProvider = (props) => {

    const { children } = props;

    return (
        <OrderContext.Provider value={useOrder(props)}>
            {children}
        </OrderContext.Provider>
    );
};

import React, {
    createContext,
    useState,
} from 'react';

import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

export const SnaccBarContext = createContext();

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export function useSnacc() {

    const [openSnacc, setOpenSnacc] = useState(false);
    const handleSnackBarClose = () => {
        setOpenSnacc(false);
    }

    // useEffect(() => {
    //     // console.log("snacc");
    //     snaccBar();
    // }, [openSnacc]);

    const snaccBar = () => {
        // console.log("show");
        return (
            <Snackbar open={openSnacc} autoHideDuration={6000} onClose={handleSnackBarClose}>
                <Alert onClose={handleSnackBarClose} severity="success">
                    Order Has sucessfully been made
                </Alert>
            </Snackbar>
        );
    }
    
    return {
        openSnacc,
        setOpenSnacc,
        snaccBar,
    };
}

export const SnaccBarProvider = (props) => {
    console.log(useSnacc(props));
    const { children } = props;
    const Snacc = useSnacc(props);

    return (
        <SnaccBarContext.Provider value={useSnacc(props)}>
            {children}
            <Snackbar open={Snacc.openSnacc} autoHideDuration={6000} onClose={Snacc.handleSnackBarClose}>
                <Alert onClose={Snacc.handleSnackBarClose} severity="success">
                    Order Has sucessfully been made
                </Alert>
            </Snackbar>
        </SnaccBarContext.Provider>
    );
};

import React from 'react';
import './styles/global.scss';
import { Landing, MenuPage, OrderListPage, Success } from './components/';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App() {
  return (
    <Router>
      <Switch>
          <Route path="/menu">
            <MenuPage/>
          </Route>
          <Route path="/order">
            <OrderListPage/>
          </Route>
          <Route path="/success" >
            <Success/>
          </Route>
          <Route path="/">
            <Landing/>
          </Route>
          <Route >
            NOT FOUND
          </Route>
        </Switch>
    </Router>
  );
}

export default App;
